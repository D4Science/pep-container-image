FROM nginx:stable

# The following variables are used to keep the ansible modifications to a minumum.

ENV DOCKER_FULL_STACK_SERVICE_NAME="localhost"
ENV DOCKER_SERVICE_PORT=8080
ENV SERVICE_HOST="localhost"
ENV KEYCLOACK_SERVER="localhost"
ENV ACCOUNTING_SERVICE_BASEURL="https://localhost"
ENV SUBREQUEST_OUTPUT_BUFFER_SIZE="8192k"
ENV SHINYPROXY_PROMETHEUS_PORT="9090"
ENV D4SCIENCE_API_ENDPOINT="https://localhost"


# config.js and nginx.default.conf.template must be overwritten at deployment time
# with the correct files for the scenario
#
COPY src/nginx.conf /etc/nginx/nginx.conf
COPY src/config.js /etc/nginx/config.js
COPY src/nginx.default.conf.template /etc/nginx/templates/default.conf.template
COPY src/pep.js /etc/nginx/pep.js

