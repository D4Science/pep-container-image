export default { config };

var config = {
    "pep_credentials": "pep_credentials",
    "debug": true,
    "accounting": {
        "scopes": ["authorized_scope1", "authorizeed_scope2"],
        "service_name": "docker_stack_name",
        "service_class": "Application",
        "host": "service_hostname"
    },
    "hosts": [
        {
            "host": ["service_hostname"],
            "audience": "oidc_client_id",
            "allow-basic-auth": "true/false",
            "paths": [
                {
                    "name": "oidc_client_resource_name",
                    "path": "^/?.*$",
                    "methods": [
                        {
                            "method": "GET"
                        },
                        {
                            "method": "POST"
                        }
                    ]
                }
            ]
        }
    ]
}

