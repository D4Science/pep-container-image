# PEP image, nginx based

This is a nginx image with some bits of a javascript based PEP.
Two files:

* nginx.default.conf.template
* config.js

must be overwritten (via ansible): `config.js` as *secret*, `nginx.default.conf.template` as *config*.

